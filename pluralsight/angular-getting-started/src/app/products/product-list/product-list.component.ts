import { Component, OnInit } from '@angular/core';

import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  title = 'Product List';
  imageWidth = 50;
  imageMargin = 2;
  showImage = false;
  errorMessage = '';
  products: Product[];
  filteredProducts: Product[];

  private _productService;
  private _listFilter: string;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.findProducts().subscribe(
      products => {
        this.products = this.filteredProducts = products;
      },
      error => this.errorMessage = <any>error
    );
  }

  onRatingClicked(message: string): void {
    this.title = 'Product List: ' + message;
  }

  performFilter(filterBy: string): Product[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.products.filter((product: Product) =>
      product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  public get listFilter(): string {
    return this._listFilter;
  }

  public set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }
}
