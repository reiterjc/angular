import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('f') 
  signupForm: NgForm;
  genders = ['male', 'female'];

  suggestUserName() {
    this.signupForm.form.patchValue({
      userData: {
        username: 'Superuser'
      }
    })
  }

  onSubmit() {
    console.log(this.signupForm);
    this.signupForm.reset();
  }
}
