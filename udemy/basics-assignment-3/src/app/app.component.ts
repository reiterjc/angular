import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  displayEnabled: boolean = false;
  events = [];

  toggleDisplayEnabled() : void {
    this.displayEnabled = ! this.displayEnabled;
    this.events.push('Display enabled: ' + this.displayEnabled + ' at ' + Date());
  }
}
