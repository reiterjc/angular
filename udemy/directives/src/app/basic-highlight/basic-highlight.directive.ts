import { Directive, HostListener, HostBinding, Input, OnInit } from "@angular/core";

@Directive({
    selector: '[appBasicHighlight]'
})
export class BasicHighlightDirective implements OnInit {

    @Input()
    defaultColor: string = 'transparent';
    @Input()
    highlightColor: string = 'blue';
    @HostBinding('style.backgroundColor')
    backgroundColor: string;

    ngOnInit() {
        this.backgroundColor = this.defaultColor;
    }

    @HostListener('mouseenter')
    onMouseEnter(eventData: Event) {
        this.backgroundColor = this.highlightColor;
    }

    @HostListener('mouseleave')
    onMouseLeave(eventData: Event) {
        this.backgroundColor = this.defaultColor;
    }
}
