import { Injectable } from "@angular/core";
import { Recipe } from "./recipe.model";
import { Ingredient } from "../shared/ingredient.model";

@Injectable({ providedIn: 'root' })
export class RecipeService {

    private recipes: Recipe[] = [
        new Recipe(
            'Mac and Cheese',
            'A super-tasty mac and cheese',
            'https://images-gmi-pmc.edge-generalmills.com/0d175828-384a-44eb-abbb-0500c07cf397.jpg',
            [
                new Ingredient('Macaroni', 4),
                new Ingredient('Cheese', 3),
                new Ingredient('Milk', 1)
            ]
        ),
        new Recipe(
            'Tacos',
            'Tacos are awesome, not much else to say',
            'https://d5dnlg5k9nac9.cloudfront.net/processed/thumbs/1be59757014205d43bfa5f60e9ad17a6ce12474b_r791_530.png',
            [
                new Ingredient('Shells', 6),
                new Ingredient('Meat', 6),
                new Ingredient('Salsa', 1)
            ]
        )
    ];

    getRecipe(index: number) {
        return this.recipes[index];
    }

    getRecipes() {
        return this.recipes.slice();
    }
}