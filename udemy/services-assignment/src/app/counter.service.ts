import { Injectable } from "../../node_modules/@angular/core";

@Injectable({providedIn: 'root'})
export class CounterService {
    counter: number = 0;

    incrementCounter() {
        console.log('incrementing counter: ' + ++this.counter);
    }
}