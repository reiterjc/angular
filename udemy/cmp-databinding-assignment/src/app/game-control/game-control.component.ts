import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  @Output()
  counterChanged = new EventEmitter<number>();
  currentCounter: number = 0;
  interval;

  constructor() { }

  ngOnInit() {
  }

  onStart() {
    this.interval = setInterval(() => {
      this.counterChanged.emit(++this.currentCounter);
    }, 1000);
  }

  onStop() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

}
